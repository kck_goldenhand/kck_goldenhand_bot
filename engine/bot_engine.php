<?php session_start(); ?>
<?php if(!isSet($_SESSION['temat']) || $_SESSION['temat'] == '') $_SESSION['temat'] = 'main'; ?>


<?php

function err($string){
    echo "[ERR_engine]: ".$string;
    die(); 
    return false;
}



//----------- Sprawdzenie zmiennych ------------------

if(!isSet($_POST['action'])) err("Brak akcji.");
if(!isSet($_POST['value'])) err("Brak wartosci.");


$action = $_POST['action'];
$action_list = array('rozmowa', 'reset_bot');
$value = $_POST['value'];

if(!in_array($action, $action_list)) err("Nieprawidlowa akcja.");



//Zalaczenie bibliotek odpowiedzi:
require_once('lib_loader.php');




//------------- Funkcje silnika -----------------------
function rozmowa($val)
{
    echo input_mess($val);
}

function reset_bot($val)
{
    if($val == true)
    {
        //resetowanie zmiennych itp
        session_unset();
        echo "Bot zrestartowany";
    }
}



//----------- Glowny Switch akcji ----------------------
//sleep(2);

switch($action){
    case 'rozmowa':
        rozmowa($value);
        break;
    case 'reset_bot':
        reset_bot($value);
        break;
        
}

?>