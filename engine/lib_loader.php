<?php session_start(); ?>

<?php

function zapiszFraze($string){
    $string .= "\n";
    $file = 'nieZnane.txt';
    file_put_contents($file, $string, FILE_APPEND | LOCK_EX);
}

function zapiszLog($string){
    $string .= "\n";
    $file = 'log.txt';
    file_put_contents($file, $string, FILE_APPEND | LOCK_EX);
}


function usunPolski($polskiString)
{
    $polish = array("ę", "ó", "ą", "ś", "ł", "ż", "ź", "ć", "ń", "Ę", "Ó", "Ą", "Ś", "Ł", "Ż", "Ź", "Ć", "Ń", "?", "!", "/", " :)", " xD", " :D", " :P");
    $alter  = array("e", "o", "a", "s", "l", "z", "z", "c", "n", "E", "O", "A", "S", "L", "Z", "Z", "C", "N", " ", " ", " ", " ",   " ",   " ",   " ");

    return str_replace($polish, $alter, $polskiString);
}


function input_mess($input_message)
{  
    $orginal_input_message = $input_message;
    $jsonDir = "scenario/";
    $match = false;
    $jsonFiles = scandir($jsonDir);
    
    
    $input_message = strtolower(trim(usunPolski($input_message)));
    
    $return = array('grupa' => 'NULL', 'id' => 'NULL', 'temat' => 'NULL');
    
    
    foreach($jsonFiles as $file)  //Szukam po temacie rozmowy wpisanej frazy w scenariuszu
    {
        if($match) break;
        
        if(!(strpos($file,'.json') !== false)) continue;
        if(!(strpos($file,'in_') !== false)) continue;
        
        $fileContents = file_get_contents("scenario/".$file);
        
        if(!(strpos(strtolower($fileContents),$input_message) !== false)) continue;
        
        $data = json_decode($fileContents, true);
        
        

        foreach($data['rozmowy'] as $wpis)
        {
            if($match) break;
            
            foreach($wpis["wejscia"] as $inputs)
            {
                if($match) break;
                
                if((strpos(strtolower(trim($inputs['in'])),$input_message) !== false) && $wpis['temat'] == $_SESSION['temat'])
                {
                    $return['grupa'] = $data['grupa'];
                    $return['id'] = $wpis['id'];
                    if(isSet($wpis['temat']))
                        $return['temat'] = $wpis['temat'];
                    
                    $match = true;
                    break;
                }
            }
        }

    }
    
	if(!$match)
    {
        foreach($jsonFiles as $file)  //Szukam po temacie rozmowy scenariusza w frazie
		{
			if($match) break;
			
			if(!(strpos($file,'.json') !== false)) continue;
			if(!(strpos($file,'in_') !== false)) continue;
			
			$fileContents = file_get_contents("scenario/".$file);
			
			//if(!(strpos($input_message,strtolower($fileContents)) !== false)) continue;
			
			$data = json_decode($fileContents, true);
			
			

			foreach($data['rozmowy'] as $wpis)
			{
				if($match) break;
				
				foreach($wpis["wejscia"] as $inputs)
				{
					if($match) break;
					
					if((strpos($input_message,strtolower(trim($inputs['in']))) !== false) && $wpis['temat'] == $_SESSION['temat'])
					{
						$return['grupa'] = $data['grupa'];
						$return['id'] = $wpis['id'];
						if(isSet($wpis['temat']))
							$return['temat'] = $wpis['temat'];
						
						$match = true;
						break;
					}
				}
			}

		}  
    }
    
    if(!$match)
    {
        foreach($jsonFiles as $file)  //Szukam bez tematu rozmowy wpisanej frazy w scenariuszu
        {
            if($match) break;

            if(!(strpos($file,'.json') !== false)) continue;
            if(!(strpos($file,'in_') !== false)) continue;

            $fileContents = file_get_contents("scenario/".$file);

            if(!(strpos(strtolower($fileContents),$input_message) !== false)) continue;

            $data = json_decode($fileContents, true);



            foreach($data['rozmowy'] as $wpis)
            {
                if($match) break;

                foreach($wpis["wejscia"] as $inputs)
                {
                    if($match) break;

                    if((strpos(strtolower(trim($inputs['in'])),$input_message) !== false))
                    {
                        $return['grupa'] = $data['grupa'];
                        $return['id'] = $wpis['id'];
                        if(isSet($wpis['temat']))
                            $return['temat'] = $wpis['temat'];

                        $match = true;
                        break;
                    }
                }
            }

        }   
    }	
		
	if(!$match)
    {
        foreach($jsonFiles as $file)  //Szukam bez tematu rozmowy scenariusza w frazie
        {
            if($match) break;

            if(!(strpos($file,'.json') !== false)) continue;
            if(!(strpos($file,'in_') !== false)) continue;

            $fileContents = file_get_contents("scenario/".$file);

            //if(!(strpos($input_message,strtolower($fileContents)) !== false)) continue;

            $data = json_decode($fileContents, true);



            foreach($data['rozmowy'] as $wpis)
            {
                if($match) break;

                foreach($wpis["wejscia"] as $inputs)
                {
                    if($match) break;

                    if((strpos($input_message, strtolower(trim($inputs['in']))) !== false))
                    {
                        $return['grupa'] = $data['grupa'];
                        $return['id'] = $wpis['id'];
                        if(isSet($wpis['temat']))
                            $return['temat'] = $wpis['temat'];

                        $match = true;
                        break;
                    }
                }
            }

        }   
    } //do tąd 
    
    if(!$match)
    { 
        zapiszLog("Gracz: ".$orginal_input_message);
        zapiszLog(" Bot: Nie rozumiem :< <i>(Debuger: Brak szukanej frazy)</i>\r\n");
        zapiszFraze($orginal_input_message."\r\n");
        //var_dump($_SESSION); 
        //echo " ";
        //var_dump($return);
        //echo " ";
        //echo "Nie rozumiem :< <i>(Debuger: Brak szukanej frazy)</i>"; 
        //var_dump($orginal_input_message);
        echo "SCRIPT[RUN_GOOGLE]=".$orginal_input_message;
    }
    else
    {
        zapiszLog("Gracz: ".$orginal_input_message);
        output_message($return);
    }
}



function output_message($values)
{
    $jsonDir = "scenario/";
    $match = false;
    $jsonFiles = scandir($jsonDir);
    
    foreach($jsonFiles as $file)
    {
        if(!(strpos($file,'.json') !== false)) continue;
        if(!(strpos($file,'out_') !== false)) continue;
        if(!(strpos($file,$values['grupa']) !== false)) continue;
        
        $fileContents = file_get_contents("scenario/".$file);
        $data = json_decode($fileContents, true);

        foreach($data['rozmowy'] as $wpis)
        {
            if($match) break;
            if($wpis['id'] == $values['id'])
            {
                //var_dump($wpis);
                
                $ilosc_odpowiedzi = count($wpis['wyjscia']);
                $odpowiedz = array_rand($wpis['wyjscia'], 1);
                
                $_SESSION['temat'] = $wpis['temat'];
                
                zapiszLog(" Bot: ".$wpis['wyjscia'][$odpowiedz]['out']."\r\n");
                echo $wpis['wyjscia'][$odpowiedz]['out'];
                //echo " --(debug: ";
                //var_dump($_SESSION);
                //echo " )";
                $match = true;

                break;
            }
        }

    }
    
    if(!$match)
    { 
        zapiszLog("Nie rozumiem :< <i>(Debuger: Brak odpowiedzi)</i>"); 
        echo "Chyba nie wiem o co Ci chodzi.";
    }

}

?>