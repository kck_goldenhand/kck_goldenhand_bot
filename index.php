<?php

    $lista_modulow = array("bot", "info", "help", "historia", "pilkahelp");

    if(!isSet($_GET['module']))
        $active_module = "bot";
    else
        if(in_array($_GET['module'], $lista_modulow))
           $active_module = $_GET['module'];
        else
           $active_module = "bot";

           
	require_once('modules/header.php');
	
	require_once('modules/main.php');

    require_once('modules/modals.php');
	
	require_once('modules/footer.php');

           
?>