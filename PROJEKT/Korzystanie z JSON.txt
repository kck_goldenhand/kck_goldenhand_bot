OPIS GLOWNY
*Dla kazdej grupy tworzymy plik in_ oraz out_
*nazwa pliku po przedrostku musi byc zgodna z "grupa": ""
*rodzaj jest wejscia dla in_ i wyjscia dla out_
*wszystkie scenariusze wpisujemy do "tablicy" rozmowy
*WAZNE: WEJSCIOWE WPISY PISZEMY MALYMI LITERAMI! ROBIMY WERSJE Z POLSKIMI ZNAKAMI WSZYSTKIMI ORAZ BEZ RZADNYCH POLSKICH ZNAKOW !
*Jeden scenariusz ma taki wyglad:
{                                        numer pliku 10 - 99  | numer alternatywy |   Numer rozmowy
    "id": "100001",              <- ID            __                    _               ___
    "id_alt": "101001",          <- ID alternatywnej odpowiedzi, wykorzystywana w razie gdy uzytkownik powtorzy jakisz scenariusz.
    "opis": "Powitania",         <- Głownie dla ulatwienia programisty, ale moze gdzies wykorzystamy
    "wejscia": [                 <- Tablica zawierajaca wszystkie "template" wiadomosci od uzytkownika, pasujacych do danego scenariusza rozmowy
        {"in":"witaj"},
        {"in":"siemanko"},
        {"in":"hej"},
        {"in":"heja"},
        {"in":"elo"},
        {"in":"czesc"},
        {"in":"cześć"}
    ] 
}
*Odpowiedz bota zgodnie z scenariuszem moze miec jedną, lub wiecej odpowiedzi (w przypadku alternatyw/powtorzen)
*Przykladowy template odpowiedzi wraz z odpowiedzia alternatywna:
-odpowiedz glowna:
{
    "id": "100001",                  <- ID scenariusza, wyszukiwany na podstawie id wejsciowego
    "opis": "Powitania",             <- Głownie dla ulatwienia programisty, ale moze gdzies wykorzystamy
    "wyjscia": [                     <- Tablica zawierajaca wszystkie "template" odpowiedzi
        {"out":"Witaj!"},
        {"out":"No cześć :)"},
        {"out":"Heja."},
        {"out":"Joł Joł."},
        {"out":"Dzień Dobry."}
    ] 
}
-Alternatywa:        
{
    "id": "101001",                     <- ID scenariusza, wyszukiwany na podstawie id wejsciowego
    "opis": "Ponowne Powitanie",        <- Głownie dla ulatwienia programisty, ale moze gdzies wykorzystamy
    "wyjscia": [                        <- Tablica zawierajaca wszystkie "template" odpowiedzi
        {"out":"Witalismy sie juz!"},
        {"out":"No cześć... znowu :)"}
    ] 
}




--------------------------------------------------------------------------------------------------------------------------------------------------------------




TEMPLATE IN
{
    "grupa": "main",
    "rodzaj": "wejscia",
    "rozmowy": [
        {
            "id": "100001",
            "id_alt": "101001",
            "opis": "Powitania",
            "wejscia": [
                {"in":"witaj"},
                {"in":"siemanko"},
                {"in":"hej"},
                {"in":"heja"},
                {"in":"elo"},
                {"in":"czesc"},
                {"in":"cześć"},
                {"in":"czesć"},
                {"in":"cześc"}
            ] 
        }
    ]
}



TEMPLATE OUT
{
    "grupa": "main",
    "rodzaj": "wyjscia",
    "rozmowy": [
        {
            "id": "100001",
            "opis": "Powitania",
            "wyjscia": [
                {"out":"Witaj!"},
                {"out":"No cześć :)"},
                {"out":"Heja."},
                {"out":"Joł Joł."},
                {"out":"Dzień Dobry."}
            ] 
        },
        {
            "id": "101001",
            "opis": "Ponowne Powitanie",
            "wyjscia": [
                {"out":"Witalismy sie juz!"},
                {"out":"No cześć... znowu :)"}
            ] 
        }
    ]
}