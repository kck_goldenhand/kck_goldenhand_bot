    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/bootstrap.min.js"></script> <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="js/menu.js"></script>
    <!-- CUSTOM SCRIPTS SECTION -->

    <?php if($active_module == "bot"): ?>
        <script type="text/javascript" src="js/tictactoe.js"></script>
        <script type="text/javascript" src="js/bot-main-script.js"></script>
    <?php endif; ?>

</html>