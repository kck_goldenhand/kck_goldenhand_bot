<div class="container">
    <div id="div_bot_section">
        <div id="div_tlo1"></div>
        <div id="div_gracz_block"></div>
        <div id="div_messages_block">
            <!-- <textarea id="area_messages_block" class="form-control" style="height: 100%;" disabled></textarea> -->
        </div>
        <div id="div_message_input" class="form-inline">
            <input id="input_message" style="width: 86%;" type="text" class="form-control" placeholder="Wiadomość..." /> 
            <button id="button_send_message" style="width: 10%;" type="button" class="btn btn-default">Wyślij</button>
        </div>
        <div id="div_tlo2"></div>
        <div id="div_bot_block"></div>
    </div>
</div>