<br /> <br />
<div class="pnoznahelp">
<h3><center>Drużyny które znam:</center></h3></br>
<!--
    Janusz GoldenHand to bot, z którym możesz porozmawiać m.in. o piłce nożnej. Jeśli masz jakieś pytanie na temat piłki nożnej lub chcesz poznań kilka ciekawostek o tym sporcie
	to dobrze trafiłeś. Aby pokazać ci kilka możliwości Janusza przygotowaliśmy ten artykuł. </br><br/>
	Janusz dysponuje wiedzą, która pozwoli Ci dowiedzieć się podstawowych informacji na temat 150 klubów. Oto one: </br> -->
</div>

<div class="resthelp_pnozna">
    <!--<h3><center>Po najechaniu na herb wyświetli Ci się nazwa klubu. W konstruowaniu pytania zalecane jest użycie właśnie jej.</center></h3>
    <br />-->
<table border="0" style="width:100%">	
<tr>
	<td align="center">
		<h3> Ekstraklasa: </h3>
	</td>
</tr>
	<td align="center">
		<img src="images/p_nozna/ekstraklasa/cracovia_krakow.jpg"  title="CRACOVIA KRAKÓW"/>
		<img src="images/p_nozna/ekstraklasa/gornik_leczna.jpg"  title="GÓRNIK ŁĘCZNA"/>
		<img src="images/p_nozna/ekstraklasa/gornik_zabrze.jpg"  title="GÓRNIK ZABRZE"/>
		<img src="images/p_nozna/ekstraklasa/jaga.jpg"  title="JAGIELLONIA BIAŁYSTOK"/>
		<img src="images/p_nozna/ekstraklasa/korona.jpg"  title="KORONA KIELCE"/>
		<img src="images/p_nozna/ekstraklasa/lech_poznan.jpg"  title="KKS LECH POZNAŃ"/>
		<img src="images/p_nozna/ekstraklasa/lechia_gdansk.jpg"  title="LECHIA GDAŃSK"/>
		<img src="images/p_nozna/ekstraklasa/legia_warszawa.jpg"  title="LEGIA WARSZAWA"/>
		<img src="images/p_nozna/ekstraklasa/piast.jpg"  title="PIAST GLIWICE"/>
		<img src="images/p_nozna/ekstraklasa/podbeskidzie.jpg"  title="PODBESKIDZIE BIELSKO-BIAŁA"/>
		<img src="images/p_nozna/ekstraklasa/pogon_szczecin.jpg"  title="POGOŃ SZCZECIN"/>
		<img src="images/p_nozna/ekstraklasa/ruch_chorzow.jpg"  title="RUCH CHORZÓW"/>
		<img src="images/p_nozna/ekstraklasa/slask_wroclaw.jpg"  title="ŚLĄSK WROCŁAW"/>
		<img src="images/p_nozna/ekstraklasa/termalika.jpg"  title="TERMALIKA NIECIECZA"/>		
		<img src="images/p_nozna/ekstraklasa/wisla_krakow.jpg"  title="WISŁA KRAKÓW"/>
		<img src="images/p_nozna/ekstraklasa/zaglebie_lubin.jpg"  title="ZAGŁĘBIE LUBIN"/>
		<img src="images/p_nozna/ekstraklasa/gks.jpg"  title="GKS BEŁCHATÓW"/>
		<img src="images/p_nozna/ekstraklasa/zawisza.jpg"  title="ZAWISZA BYDGOSZCZ"/>
	</td>			
</tr>
<tr>
	<td align="center">
		<h3> Premier League: </h3>
	</td> 
</tr>
<tr>
<tr><td align="center">
		<img src="images/p_nozna/premier_league/city.jpg"  title="MANCHESTER CITY"/>
		<img src="images/p_nozna/premier_league/man_utd.jpg"  title="MANCHESTER UNITED"/>
		<img src="images/p_nozna/premier_league/liverpool.jpg"  title="LIVERPOOL"/>
		<img src="images/p_nozna/premier_league/arsenal.jpg"  title="ARSENAL LONDYN"/>
		<img src="images/p_nozna/premier_league/chelsea.jpg"  title="CHELSEA LONDYN"/>
		<img src="images/p_nozna/premier_league/west_brom.jpg"  title="WEST BROM"/>		
		<img src="images/p_nozna/premier_league/newcastle.jpg"  title="NEWCASTLE UNITED"/>
		<img src="images/p_nozna/premier_league/leicester.jpg"  title="LEICESTER CITY"/>
		<img src="images/p_nozna/premier_league/watford.jpg"  title="WATFORD"/>		
		<img src="images/p_nozna/premier_league/spurs.jpg"  title="TOTTENHAM HOTSPUR"/>
		<img src="images/p_nozna/premier_league/west_ham.jpg"  title="WEST HAM"/>
		<img src="images/p_nozna/premier_league/southampton.jpg"  title="SOUTHAMPTON"/>
		<img src="images/p_nozna/premier_league/stoke.jpg"  title="STOKE CITY"/>
		<img src="images/p_nozna/premier_league/cr_palace.jpg"  title="CRYSTAL PALACE"/>
		<img src="images/p_nozna/premier_league/sunderland.jpg"  title="SUNDERLAND"/>
		<img src="images/p_nozna/premier_league/swansea.jpg"  title="SWANSEA"/>
		<img src="images/p_nozna/premier_league/norwich.jpg"  title="NORWICH CITY"/>		
		<img src="images/p_nozna/premier_league/everton.jpg"  title="EVERTON"/>
		<img src="images/p_nozna/premier_league/avilla.jpg"  title="ASTON VILLA"/>
		<img src="images/p_nozna/premier_league/bournemouth.jpg"  title="BOURNEMOUTH"/>
		<img src="images/p_nozna/premier_league/hull.jpg"  title="HULL CITY"/>
		<img src="images/p_nozna/premier_league/burnley.jpg"  title="BURNLEY"/>
		<img src="images/p_nozna/premier_league/qpr.jpg"  title="QUEENS PARK RANGERS"/>
	</td> 
</tr>
<tr> 
	<td align="center">
		<h3> Serie A: </h3>
	</td>  
</tr>
<tr>
	<td align="center">
		<img src="images/p_nozna/serie_a/fiorentina.jpg"  title="FIORENTINA"/>
		<img src="images/p_nozna/serie_a/inter.jpg"  title="INTER MEDIOLAN"/>
		<img src="images/p_nozna/serie_a/roma.jpg"  title="AS ROMA"/>
		<img src="images/p_nozna/serie_a/napoli.jpg"  title="SSC NAPOLI"/>
		<img src="images/p_nozna/serie_a/sassuolo.jpg"  title="SASSUOLO"/>
		<img src="images/p_nozna/serie_a/milan.jpg"  title="AC MILAN"/>
		<img src="images/p_nozna/serie_a/juventus.jpg"  title="JUVENTUS TURYN"/>
		<img src="images/p_nozna/serie_a/atalanta.jpg"  title="ATALANTA"/>
		<img src="images/p_nozna/serie_a/lazio.jpg"  title="LAZIO RZYM"/>
		<img src="images/p_nozna/serie_a/sampdoria.jpg"  title="SAMPDORIA"/>
		<img src="images/p_nozna/serie_a/torino.jpg"  title="TORINO"/>
		<img src="images/p_nozna/serie_a/palermo.jpg"  title="PALERMO"/>
		<img src="images/p_nozna/serie_a/empoli.jpg"  title="EMPOLI"/>
		<img src="images/p_nozna/serie_a/chievo.jpg"  title="CHIEVO VERONA"/>
		<img src="images/p_nozna/serie_a/genoa.jpg"  title="GENOA"/>
		<img src="images/p_nozna/serie_a/bologna.jpg"  title="BOLOGNA"/>
		<img src="images/p_nozna/serie_a/udine.jpg"  title="UDINESE CALCIO"/>
		<img src="images/p_nozna/serie_a/frosinone.jpg"  title="FROSINONE"/>
		<img src="images/p_nozna/serie_a/hellas.jpg"  title="HELLAS VERONA"/>
		<img src="images/p_nozna/serie_a/carpi.jpg"  title="CARPI"/>
		<img src="images/p_nozna/serie_a/cagliari.jpg"  title="CAGLIARI"/>
		<img src="images/p_nozna/serie_a/cesena.jpg"  title="CESENA"/>
		<img src="images/p_nozna/serie_a/parma.jpg"  title="PARMA"/>
	</td>	
</tr>
<tr>
	<td align="center">
		<h3>Primera Division: </h3>
	</td>
</tr>
<tr>
	<td align="center">
		<img src="images/p_nozna/primera_division/barca.jpg"  title="FC BARCELONA"/>
		<img src="images/p_nozna/primera_division/real.jpg"  title="REAL MADRYT"/>
		<img src="images/p_nozna/primera_division/atletico.jpg"  title="ATLETICO MADRYT"/>
		<img src="images/p_nozna/primera_division/valencia.jpg"  title="VALENCIA"/>
		<img src="images/p_nozna/primera_division/sevilla.jpg"  title="SEVILLA"/>
		<img src="images/p_nozna/primera_division/villareal.jpg"  title="VILLARREAL"/>
		<img src="images/p_nozna/primera_division/bilbao.jpg"  title="ATHLETIC BILBAO"/>
		<img src="images/p_nozna/primera_division/celta_vigo.jpg"  title="CELTA VIGO"/>
		<img src="images/p_nozna/primera_division/malaga.jpg"  title="MALAGA"/>
		<img src="images/p_nozna/primera_division/espanyol.jpg"  title="ESPANYOL"/>
		<img src="images/p_nozna/primera_division/vallecano.jpg"  title="RAYO VALLECANO"/>
		<img src="images/p_nozna/primera_division/sociedad.jpg"  title="REAL SOCIEDAD"/>
		<img src="images/p_nozna/primera_division/levante.jpg"  title="LEVANTE"/>
		<img src="images/p_nozna/primera_division/getafe.jpg"  title="GETAFE"/>
		<img src="images/p_nozna/primera_division/deportivo.jpg"  title="DEPORTIVO LA CORUNA"/>
		<img src="images/p_nozna/primera_division/granada.jpg"  title="GRANADA"/>
		<img src="images/p_nozna/primera_division/eibar.jpg"  title="EIBAR"/>
		<img src="images/p_nozna/primera_division/betis.jpg"  title="BETIS"/>
		<img src="images/p_nozna/primera_division/gijon.jpg"  title="SPORTING GIJON"/>
		<img src="images/p_nozna/primera_division/las_palmas.jpg"  title="LAS PALMAS"/>
		<img src="images/p_nozna/primera_division/elche.jpg"  title="ELCHE"/>
		<img src="images/p_nozna/primera_division/almeria.jpg"  title="ALMERIA"/>
		<img src="images/p_nozna/primera_division/cordoba.jpg"  title="CORDOBA"/>
	</td>
</tr>
<tr>
	<td align="center">
		<h3>Bundesliga: </h3>
	</td>
</tr>
<tr>
	<td align="center">
		<img src="images/p_nozna/bundesliga/bayern.jpg"  title="BAYERN MONACHIUM"/>
		<img src="images/p_nozna/bundesliga/dortmund.jpg"  title="BORUSSIA DORTMUND"/>
		<img src="images/p_nozna/bundesliga/schalke.jpg"  title="SCHALKE 04"/>
		<img src="images/p_nozna/bundesliga/wolfsburg.jpg"  title="WOLFSBURG"/>
		<img src="images/p_nozna/bundesliga/bor_moench.jpg"  title="BORUSSIA MONCHENGLADBACH"/>
		<img src="images/p_nozna/bundesliga/bayer.jpg"  title="BAYER LEVERKUSEN"/>
		<img src="images/p_nozna/bundesliga/augsburg.jpg"  title="AUGSBURG"/>
		<img src="images/p_nozna/bundesliga/hoffenheim.jpg"  title="HOFFENHEIM"/>
		<img src="images/p_nozna/bundesliga/frankfurt.jpg"  title="FRANKFURT"/>
		<img src="images/p_nozna/bundesliga/bremen.jpg"  title="WERDER BREMA"/>
		<img src="images/p_nozna/bundesliga/mainz.jpg"  title="MAINZ"/>
		<img src="images/p_nozna/bundesliga/koeln.jpg"  title="KOELN"/>
		<img src="images/p_nozna/bundesliga/hannover.jpg"  title="HANNOVER 96"/>
		<img src="images/p_nozna/bundesliga/stuttgart.jpg"  title="VfB STUTTGART"/>
		<img src="images/p_nozna/bundesliga/hertha.jpg"  title="HERTHA"/>
		<img src="images/p_nozna/bundesliga/hsv.jpg"  title="HSV"/>
		<img src="images/p_nozna/bundesliga/freiburg.jpg"  title="FREIBURG"/>
		<img src="images/p_nozna/bundesliga/ingolstadt.jpg"  title="INGOLSTADT"/>
		<img src="images/p_nozna/bundesliga/darmstadt.jpg"  title="DARMSTADT"/>
		<img src="images/p_nozna/bundesliga/paderborn.jpg"  title="PADERBORN"/>
	</td>
</tr>
<tr>
	<td align="center">
		<h3>Ligue 1: </h3>
	</td>
</tr>
<tr>
	<td align="center">		
		<img src="images/p_nozna/ligue_1/psg.jpg"  title="PARIS SAINT-GERMAIN"/>
		<img src="images/p_nozna/ligue_1/lyon.jpg"  title="OLYMPIQUE LYON"/>
		<img src="images/p_nozna/ligue_1/monaco.jpg"  title="AS MONACO"/>
		<img src="images/p_nozna/ligue_1/marseille.jpg"  title="OLYMPIQUE MARSYLIA"/>
		<img src="images/p_nozna/ligue_1/st_etienne.jpg"  title="SAINT-ETIENNE"/>
		<img src="images/p_nozna/ligue_1/bordeaux.jpg"  title="GIRONDINS BORDEAUX"/>
		<img src="images/p_nozna/ligue_1/montpellier.jpg"  title="MONTPELLIER"/>
		<img src="images/p_nozna/ligue_1/lille.jpg"  title="OSC LILLE"/>
		<img src="images/p_nozna/ligue_1/rennes.jpg"  title="RENNES"/>
		<img src="images/p_nozna/ligue_1/guingamp.jpg"  title="GUINGAMP"/>
		<img src="images/p_nozna/ligue_1/nice.jpg"  title="NICE"/>
		<img src="images/p_nozna/ligue_1/bastia.jpg"  title="BASTIA"/>
		<img src="images/p_nozna/ligue_1/caen.jpg"  title="CAEN"/>
		<img src="images/p_nozna/ligue_1/nantes.jpg"  title="NANTES"/>
		<img src="images/p_nozna/ligue_1/reims.jpg"  title="STADE REIMS"/>
		<img src="images/p_nozna/ligue_1/lorient.jpg"  title="LORIENT"/>
		<img src="images/p_nozna/ligue_1/toulouse.jpg"  title="TOULOUSE"/>
		<img src="images/p_nozna/ligue_1/troyes.jpg"  title="TROYES"/>
		<img src="images/p_nozna/ligue_1/angers.jpg"  title="ANGERS"/>
		<img src="images/p_nozna/ligue_1/gfc_ajaccio.jpg"  title="GFC AJACCIO"/>
		<img src="images/p_nozna/ligue_1/evian.jpg"  title="EVIAN TG"/>
		<img src="images/p_nozna/ligue_1/metz.jpg"  title="METZ"/>
		<img src="images/p_nozna/ligue_1/lens.jpg"  title="LENS"/>
	</td>
</tr>
<tr>
	<td align="center">
		<h3>oraz: </h3>
	</td>
</tr>
<tr>
	<td align="center">
		<img src="images/p_nozna/other/benfica.jpg"  title="BENFICA LIZBONA"/>
		<img src="images/p_nozna/other/porto.jpg"  title="FC PORTO"/>
		<img src="images/p_nozna/other/zenit.jpg"  title="ZENIT PETERSBURG"/>
		<img src="images/p_nozna/other/basel.jpg"  title="FC BASEL"/>
		<img src="images/p_nozna/other/olimpiakos.jpg"  title="OLYMPIAKOS PIRAEUS"/>
		<img src="images/p_nozna/other/szachtar.jpg"  title="SZACHTAR DONIECK"/>
		<img src="images/p_nozna/other/galata.jpg"  title="GALATASARAY SK"/>
		<img src="images/p_nozna/other/dynamokijow.jpg"  title="DYNAMO KIJÓW"/>
		<img src="images/p_nozna/other/cska.jpg"  title="CSKA MOSKWA"/>
		<img src="images/p_nozna/other/sporting_lizbona.jpg"  title="SPORTING LIZBONA "/>
		<img src="images/p_nozna/other/psv.jpg"  title="PSV EINDHOVEN "/>
		<img src="images/p_nozna/other/anderlecht.jpg"  title="ANDERLECHT"/>
		<img src="images/p_nozna/other/salzburg.jpg"  title="RED BULL SALZBURG "/>
		<img src="images/p_nozna/other/ajax.jpg"  title="AJAX AMSTERDAM "/>
		<img src="images/p_nozna/other/club_brugge.jpg"  title="CLUB BRUGGE "/>
		<img src="images/p_nozna/other/celtic.jpg"  title="CELTIC GLASGOW "/>
		<img src="images/p_nozna/other/apoel.jpg"  title="APOEL NIKOZJA "/>
		<img src="images/p_nozna/other/fener.jpg"  title="FENERBAHCE STAMBUŁ "/>
		<img src="images/p_nozna/other/feyenoord.jpg"  title="FEYENOORD"/>
		<img src="images/p_nozna/other/gent.jpg"  title="KAA GENT "/>
		<img src="images/p_nozna/other/dnipro.jpg"  title="DNIPRO DNIEPROPETROWSK "/>
		<img src="images/p_nozna/other/besiktas.jpg"  title="BESIKTAS JK "/>
		<img src="images/p_nozna/other/bate.jpg"  title="BATE BORYSÓW"/>
	</td>
</tr>
</table>
<br/>
<!-- Janusz Goldenhand zna odpowiedzi na temat: </h3>
<h4>1) obecnie zatrudnionego trenera, </h4>
<h4>2) daty założenia klubu, </h4>
<h4>3) miejsca, w którym zlokalizowana jest siedziba klubu, </h4>
<h4>4) stadionu, na którym klub rozgrywa swoje mecze, </h4>
<h4>5) ligii, w której obecnie występuje drużyna, </h4>
<h4>6) oficjalnej strony internetowej klubu, strony transfermaktu, na której można dowiedzieć się pozoztałych informacji o klubie, </h4>
<h4>7) obecnej formy i miejsca w tabeli, </h4>
<h4>8) oraz jakie zespoły w obecnym sezonie występują w najsilniejszych ligach Europy (wyżej wymienionych), </h4>
</br></br>-->
</div>