
<!-- Modal for test-->
<div id="modalGameTic" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Kółko-Krzyżyk</h4>
      </div>
      <div class="modal-body">
        <div class='ttt-main-container'>
            <p class='ttt-clear-float'></p>
            <div class='ttt-div ttt-1-1'><button type="button" id="button-ttt-1" class="btn btn-default button-ttt" disabled></button></div>
            <div class='ttt-div ttt-1-2'><button type="button" id="button-ttt-2" class="btn btn-default button-ttt" disabled></button></div>  
            <div class='ttt-div ttt-1-3'><button type="button" id="button-ttt-3" class="btn btn-default button-ttt" disabled></button></div>
            <p class='ttt-clear-float'></p>
            <div class='ttt-div ttt-2-1'><button type="button" id="button-ttt-4" class="btn btn-default button-ttt" disabled></button></div>
            <div class='ttt-div ttt-2-2'><button type="button" id="button-ttt-5" class="btn btn-default button-ttt" disabled></button></div>  
            <div class='ttt-div ttt-2-3'><button type="button" id="button-ttt-6" class="btn btn-default button-ttt" disabled></button></div>
            <p class='ttt-clear-float'></p>
            <div class='ttt-div ttt-3-1'><button type="button" id="button-ttt-7" class="btn btn-default button-ttt" disabled></button></div>  
            <div class='ttt-div ttt-3-2'><button type="button" id="button-ttt-8" class="btn btn-default button-ttt" disabled></button></div>  
            <div class='ttt-div ttt-3-3'><button type="button" id="button-ttt-9" class="btn btn-default button-ttt" disabled></button></div>
            <p class='ttt-clear-float'></p>
        </div>
          <p class='ttt-clear-float'></p>
      </div>
      <div class="modal-footer">
          <button id="button-ttt-player-start" type="button" class="btn btn-info" style="float: left;">Ja zaczynam</button>
          <button id="button-ttt-cpu-start" type="button" class="btn btn-info" style="float: left;">Ty zaczynasz</button>
          <button id="button-ttt-end" type="button" class="btn btn-warning" data-dismiss="modal">Kończymy</button>
      </div>
    </div>

  </div>
</div>