<?php
    function check_active($active_module, $name){
        if($active_module == $name)
            echo "active";
        else
            echo "inactive";
    }
?>


<nav class="navbar navbar-default" role="navigation">
    
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
    </button>    
  </div>
    
  <a class="navbar-brand" href="#">Janusz GoldenHand</a>
  <div class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-left">
        <li class="<?php check_active($active_module, "bot"); ?>"><a href="?module=bot">BOT<span class="sr-only">(current)</span></a></li>
        <li class="<?php check_active($active_module, "historia"); ?>"><a href="?module=historia"><span>O Mnie</span></a></li>
        <li class="<?php check_active($active_module, "info"); ?>"><a href="?module=info"><span>Info</span></a></li>
		<li class="<?php check_active($active_module, "pilkahelp"); ?>"><a href="?module=pilkahelp"><span>Drużyny piłkarskie</span></a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"> </span> Opcje <span class="caret"> </span></a>
          <ul class="dropdown-menu">
            <li id="color_black"><a href="#">Kolor Czarny</a></li>
            <li id="color_blue"><a href="#">Kolor Niebieski</a></li>
            <li id="color_green"><a href="#">Kolor Zielony</a></li>
            <li role="separator" class="divider"></li>
            <li id="run_tictactoe"><a href="#">Uruchom TicTacToe</a></li>
            <li role="separator" class="divider"></li>
            <li id="reset_button"><a href="#">Resetuj</a></li>
          </ul>
        </li>
    </ul>
  </div>
</nav>