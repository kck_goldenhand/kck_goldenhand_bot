<body>
<?php require_once('modules/menu.php'); ?>
    
    <div style="width: 100%; height: 100%;">
        <?php
            switch($active_module){
                case 'bot': include_once('modules/bot.php');
                    break;
                    
                case 'info': include_once('modules/info.php');
                    break;
                    
                case 'help': include_once('modules/help.php');
                    break;
                    
                case 'historia': include_once('modules/historia.php');
                    break;
					
				case 'pilkahelp': include_once('modules/pnozna.php');
					break;					
                    
                default: include_once('modules/bot.php');
                    break;
            }
        ?>
    </div>
    
</body>