﻿<div class="container" style="font-size: 20px; padding: 75px;">
    <h3>ChatBot Janusz Goldenhand</br></h3>
	<h4>
    Chatbot zwany inaczej Janusz Goldenhand to wirtualny asystent, zadaniem którego jest rozmowa z internautą przebywającym na stronie internetowej. 
	Rozmowa ma charakter tradycyjnego czatu tekstowego. Docelowo chatbot może pełnić najróżniejsze funkcje.</br></br>

	W rzeczywistości jest to program komputerowy działający on-line, pewien system, zbiór algorytmów odpowiadających za prowadzenie konwersacji i wszystkie działania z nią związane.</br></br>

	Jego zasadniczym zadaniem jest rozmowa z internautami. Chatbot dzięki swojemu oprogramowaniu rozpoznaje zadane pytanie i stara się dopasować odpowiedź. 
	W przypadku, gdy pytanie zadane przez użytkownika jest niezrozumiałe (lub po prostu nie ma na nie odpowiedzi w bazie wiedzy) chatbot stara się dopasować jedną z wypowiedzi 'awaryjnych'.</br></br>

	Posiada pewną wiedzę z danego zakresu, przykładowo na temat piłki nożnej, filmów i muzyki. 
	Chatbot korzysta z własnych baz danych, ma także możliwość czerpania informacji z zewnętrznych źródeł (inne bazy danych, wyszukiwarki, informacyjne strony www).</br></br>

	Janusz Goldenhand nie tylko udziela odpowiedzi na pytania - sam stara się prowadzić rozmowę.	
</div>