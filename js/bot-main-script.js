/*!
 * Glowny kod skryptu bota,
 * odpowiada za wszelkie dzialania
 * po stronie uzytkownika, takie jak animacje,
 * requesty ajax, wyswietlanie skecji bota.
 */


//------------------ZMIENNE-------------------------




//-------------------FUNKCJE------------------------

function init(){
    //jQuery('#div_bot_block').append('Avatar bot\'a');
    jQuery('#div_messages_block').append('');
}

function disable_send_message(boolean){
    if(boolean){
        $("#input_message").attr("disabled", true);
        $("#button_send_message").attr("disabled", true);
    } else {
        $("#input_message").attr("disabled", false);
        $("#button_send_message").attr("disabled", false);
    }
}


function send_message(message){
    disable_send_message(true);
    jQuery('#div_messages_block').append('<b style="color: rgb(150,50,180);">&nbsp;&nbsp;Ty:</b> ' + message + '</br>').fadeIn; 
    jQuery('#div_messages_block').animate({ scrollTop: jQuery('#div_messages_block').height() }, "slow");
    call_engine('rozmowa', message, true, botMessage);
    return true;
}

function botMessage(message){
    jQuery('#div_messages_block').append('<b style="color: rgb(10,10,10);">Bot: </b>' + message + '</br>').fadeIn; 
    jQuery('#div_messages_block').animate({ scrollTop: jQuery('#div_messages_block').height() }, "slow");
    disable_send_message(false);
    jQuery('#input_message').focus();
    return true;
}

function specialMessage(message){
    if(message == 'SCRIPT[RUN_TICTACTOE]'){
        $("#modalGameTic").modal();
        botMessage('No to gramy !');
        return true;
    }
    else if(message.substring(0, 19) == 'SCRIPT[RUN_GOOGLE]='){
        var find = message.substring(19);
        find = find.split(' ').join('+');
        var url = "http://lmgtfy.com/?q=" + find;
        botMessage('Chyba nie zrozumialem pytania, ale Let Me Google that for you :)');
        setTimeout(function(){
            window.open(url,"_blank", "resizable=yes, scrollbars=yes, titlebar=yes, width=800, height=900, top=10, left=10");
        }, 1500);
        return true;
    }
    else
        return false;
}


//AJAX's:
function call_engine(action, value, async, end_func){
    jQuery.ajax({
      method: "POST",
      url: "engine/bot_engine.php",
      data: { action: action, value: value },
      async: async,
      success: function (response) {
        response = response.trim();
        console.log(response);
        if(!specialMessage(response))
            end_func(response);
      }
    });
}

//-------------Funkcje automatyczne (onclick)---------

jQuery('#input_message').keypress(function( event ) {
    if(jQuery('#input_message').val().trim() == "") return true;
    if ( event.which == 13 ) {
        send_message(jQuery('#input_message').val());
        jQuery('#input_message').val('');
        return true;
    }
  });


jQuery('#button_send_message').click(function() {
    jQuery('#input_message').focus();
    if(jQuery('#input_message').val().trim() == "") return true;
    send_message(jQuery('#input_message').val());
    jQuery('#input_message').val('');      
});





//----------------------INIT----------------------------

init();
