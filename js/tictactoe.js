//-------------FrontEnd functions !!!-----------------------------

var tablica = new Array();

function init_game_buttons(){
    jQuery('.ttt-div button').prop('disabled', false);
    jQuery('.ttt-div button').html('');
    tablica = tablica.slice(0, 0);
    for(var i = 0; i < 9; i++)
    {
        tablica[i] = ' ';   
    }
    
}

function game_is_end(whoWIN){    
    jQuery('.ttt-div button').prop('disabled', true);

    if(whoWIN == 'cpu')    
        alert('Wygralem ^_^');
    else if(whoWIN == 'player') //Niemożliwe, ale niech bedzie ze nie bylo <3
        alert('Brawo, oszukałeś mnie. Chyle czoła. \n (Programista pozdrawia.)');
    else if(whoWIN == 'remis')
        alert('Remis, brawo :)');
}


function start_game(whoStart){
    init_game_buttons();
    if(whoStart == "cpu")
        ruch_cpu();
}

jQuery('#button-ttt-player-start').on('click', function(){
    start_game('player');
});


jQuery('#button-ttt-cpu-start').on('click', function(){
    start_game('cpu');
});


//Buttons functions:
jQuery('.ttt-div button').on('click', function(){
    jQuery(this).prop('disabled', true);
    jQuery(this).html('X');
    var id;
    id = jQuery(this).attr('id');
    id = id.substr(id.length - 1);
    tablica[id-1] = 'x';
    
    if(!checkWIN())
        ruch_cpu();
});

jQuery('#button-ttt-end').on('click', function(){
    jQuery('.ttt-div button').prop('disabled', true);
    jQuery('.ttt-div button').html('');
});

function pokaz_ruch_cpu(gdzie){
    tablica[gdzie] = 'o';
    console.log(tablica);
    var id = '#button-ttt-' + String(parseInt(gdzie) + 1);
    console.log(id);
    jQuery(id).prop('disabled', true);
    jQuery(id).html('O');
}

//--------------Funkcje gry / myslenie bota:-----------------------
function checkWIN(){
    var jsonString = JSON.stringify(tablica);
    var koniec_gry = false;
    $.ajax({
        type: "POST",
        url: "engine/tictactoe.php",
        data: {action: 'check_win', data : jsonString}, 
        cache: false,
        async: false
    }).done(function( msg ) {
        console.log("Sprawdzam ajaxem czy ktos wygral");
        console.log("Odpowiedz: " + msg);
        if(msg == 'player'){
            koniec_gry = true;
            game_is_end('player');
        }
        if(msg == 'cpu'){
            koniec_gry = true;
            game_is_end('cpu');
        }
        if(msg == 'remis'){
            koniec_gry = true;
            game_is_end('remis');
        }
      });
    return koniec_gry;
}


function ruch_cpu(){
    var jsonString = JSON.stringify(tablica);
    console.log(jsonString);
    $.ajax({
        type: "POST",
        url: "engine/tictactoe.php",
        data: {action: 'cpu_move', data : jsonString}, 
        cache: false,
        async: false
    }).done(function( msg ) {
        console.log("Pytam ajaxem o ruch kompa");
        console.log("Odpowiedz: " + msg);
        if(msg != 'WTF')
            pokaz_ruch_cpu(msg);
      });
    
    checkWIN();
    //console.log('CPU move');
}