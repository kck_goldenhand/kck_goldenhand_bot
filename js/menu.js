/*!
 * Skrypt odpowiadajacy za funkcje w menu
 */


jQuery('#reset_button').on('click', function(){
    jQuery('#div_messages_block').html('');
    jQuery('#input_message').val('');
    
    $.ajax({
      method: "POST",
      url: "engine/bot_engine.php",
      data: { action: "reset_bot", value: true }
    })
      .done(function( msg ) {
        console.log( "Reset action sended. Resoult: " + msg );
      });
});


jQuery('#color_black').click(function() {
    jQuery('#div_messages_block').css("color", "black");
});

jQuery('#color_blue').click(function() {
    jQuery('#div_messages_block').css("color", "blue");
});

jQuery('#color_green').click(function() {
    jQuery('#div_messages_block').css("color", "green");
});

jQuery('#run_tictactoe').click(function() {
    $("#modalGameTic").modal();
});